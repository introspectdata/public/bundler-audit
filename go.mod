module gitlab.com/gitlab-org/security-products/analyzers/bundler-audit/v2

require (
	github.com/urfave/cli v1.20.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.1.6
)
