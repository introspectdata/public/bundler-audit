package main

import (
	"bytes"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"

	"github.com/urfave/cli"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	cmd := exec.Command("bundle", "audit", "check", "--update", "--quiet")
	cmd.Dir = path
	cmd.Env = os.Environ()
	cmd.Stderr = os.Stderr

	output, err := cmd.Output()
	if err != nil {
		// bundler-audit exits with code 1 when they are vulnerabilities,
		// so ignore the exit code if the output contains "Vulnerabilities found!".
		if !strings.Contains(string(output), "Vulnerabilities found!") {
			if _, err := os.Stdout.Write(output); err != nil {
				log.Println(err)
			}
			return nil, err
		}
	}
	return ioutil.NopCloser(bytes.NewReader(output)), nil
}
