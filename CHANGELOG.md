# bundler-audit analyzer changelog

## v2.1.2
- Upgrade common to v.2.1.6

## v2.1.1
- Upgrade common to v.2.1.4, add remediations key and stable sort

## v2.1.0
- Upgrade to bundler-audit 0.6.1, bundler 2.0.1 (Jorn van de beek)

## v2.0.0
- Switch to new report syntax with `version` field

## v1.1.0
- Add dependency (package name and version) to report
- Improve compare key, remove vulnerability name

## v1.0.0
- Initial release
